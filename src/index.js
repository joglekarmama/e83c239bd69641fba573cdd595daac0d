import './index.css';
import './bootstrap.min.css';
import './js/mojs.core.js';
import config from 'visual-config-exposer';
(function () {
  var timeouts = [];
  var messageName = "zero-timeout-message";

  function setZeroTimeout(fn) {
    timeouts.push(fn);
    window.postMessage(messageName, "*");
  }

  function handleMessage(event) {
    if (event.source == window && event.data == messageName) {
      event.stopPropagation();
      if (timeouts.length > 0) {
        var fn = timeouts.shift();
        fn();
      }
    }
  }

  document.addEventListener("keydown", function (e) {
    if (gameStarted == false && gameEndScreen == false) {
      if ((e.keyCode == 39 || e.keyCode == 68) && gameEndScreen == false) {
        startGame();
      }
    }

  }, true);
  document.getElementById("btnPlay").addEventListener("click", startGame);
  document.getElementById("btnLeaderBoard").addEventListener("click", showLeaderBoard);
  document.getElementById("btnPlayAgain").addEventListener("click", startGame);
  document.getElementById("btnSound").addEventListener("click", toggleSound);
  document.getElementById("btnBackToMainMenu").addEventListener("click", showHomeScreen);

  window.addEventListener("message", handleMessage, true);

  window.setZeroTimeout = setZeroTimeout;

  window.mobileCheck = function () {
    let check = false;
    (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
  };

  window.mobileAndTabletCheck = function () {
    let check = false;
    (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
  };
  setImages();

})();

let sound = function (src) {
  this.sound = document.createElement("audio");
  this.sound.src = src;
  this.sound.setAttribute("preload", "auto");
  this.sound.setAttribute("controls", "none");
  this.sound.style.display = "none";

  document.body.appendChild(this.sound);
  this.play = function () {
    this.sound.play();
  }
  this.stop = function () {
    this.sound.pause();
  }
}

let loadImages = function (sources, callback) {
  var nb = 0;
  var loaded = 0;
  var imgs = {};
  for (var i in sources) {
    nb++;
    imgs[i] = new Image();
    imgs[i].src = sources[i];
    imgs[i].onload = function () {
      loaded++;
      if (loaded == nb) {
        callback(imgs);
      }
    }
  }
}

let timer = null;
let FPS = 90;
let game = null;
let gameStarted = false;
let highScore = null;
let canvas = null;
let images = {
  background: config.settings.bg_image,
  rock: config.settings.enemy_1,
  rock1: config.settings.enemy_2,
  rock2: config.settings.enemy_3,
  gift: "./assets/gift.png",
  boostLife: "./assets/boost-life.png",
  cannon: config.settings.shooter_1,
  cannon2: config.settings.shooter_2,
  bullet: "./assets/bulet.png",
  life: "./assets/life/life.png",
  emptyLife: "./assets/life-empty/life-empty.png",
  title: config.settings.game_logo,
  timer: "./assets/timer/timer.png",
  music: "./assets/music/music.png",
  play: "./assets/play/play@2x.png",
  swipe: "./assets/swipe/swipe@2x.png",
  pointBg: "./assets/point-bg.png"
};
let soundStatus = true;
let homepageScreen = false;
let gameEndScreen = false;
let colors = {
  cannon: "red",
  bullet: "white",
  surface: "#cccccc",
  rock: [
    "#264b96",
    "#27b376",
    "#006f3c",
    "#f9a73e",
    "#bf212f",
  ]
};

let totalLife = 3;


let sounds = {
  background: new sound("./assets/8bitwin.mp3"), // Credits to https://www.youtube.com/watch?v=vX1xq4Ud2z8
  explosion: new sound("./assets/explosion.mp3") // https://www.freesoundeffects.com/free-sounds/explosion-10070/
}
const OBJECT_CIRCLE = 0;
const OBJECT_RECT = 1;
let speed = function (fps) {
  FPS = parseInt(fps);
}
let burst = {};

function setImages() {
  let offsetWindowHeight = (window.innerHeight - 72);
  if (window.mobileAndTabletCheck()) {
    document.querySelector(".game-container").setAttribute("style", "width:" + innerWidth + "px;height:" + offsetWindowHeight + "px;background-image: url('" + config.settings.bg_image + "')");
    document.querySelector("#game").setAttribute("style", "width:" + innerWidth + "px;height:" + offsetWindowHeight + "px;");
  } else {
    document.querySelector(".game-container").setAttribute("style", "width:" + (offsetWindowHeight) + "px;height:" + offsetWindowHeight + "px;background-image: url('" + config.settings.bg_image + "')");
  }
  document.querySelector(".titleImg").setAttribute("src", config.settings.game_logo);

}
function showLeaderBoard() {
  hideHomeScreen();
  hideGameScreen();
  hideEndScreen();
  document.querySelector(".leader-board").setAttribute("style", "display:block;");
  document.querySelector(".backToMainMenu").setAttribute("style", "display:block;");
}
function showHomeScreen() {
  hideLeaderBoard();
  hideGameScreen();
  hideEndScreen();
  document.querySelector(".homescreenElement").setAttribute("style", "display:block;");
}
function showEndScreen() {
  hideHomeScreen();
  hideLeaderBoard();
  hideGameScreen();
  // document.querySelector('#gameScore').innerHTML = this.score;
  document.querySelector(".endScreenElements").setAttribute("style", "display:block;");
}
function hideEndScreen() {
  document.querySelector(".endScreenElements").setAttribute("style", "display:none;");
}

function hideLeaderBoard() {
  document.querySelector(".leader-board").setAttribute("style", "display:none;");
  document.querySelector(".backToMainMenu").setAttribute("style", "display:none;");
}

function hideHomeScreen() {
  document.querySelector(".homescreenElement").setAttribute("style", "display:none;");
}
function toggleSound() {
  if (soundStatus === true) {
    muteAudio();
    soundStatus = false;
  } else {
    unMuteAudio();
    soundStatus = true;
  }
}

function showGameScreen(score) {
  hideHomeScreen();
  hideLeaderBoard();
  hideEndScreen();
  document.querySelector(".gameScreenElements").setAttribute("style", "display:block;");
}
function hideGameScreen() {
  document.querySelector(".gameScreenElements").setAttribute("style", "display:none;");
}
function muteAudio() {
  var audios = document.getElementsByTagName('audio'),
    i, len = audios.length;
  for (i = 0; i < len; i++) {
    audios[i].muted = true;
  }

}

function unMuteAudio() {
  var audios = document.getElementsByTagName('audio'),
    i, len = audios.length;
  for (i = 0; i < len; i++) {
    console.log(audios[i]);
    audios[i].muted = false;
  }

}

class GameObject {
  constructor(objectType, center, velocity, acceleration, color, image = undefined) {
    this.center = center.slice(0);
    this.velocity = velocity;
    this.acceleration = acceleration;
    this.type = objectType;
    this.color = color;
    this.image = image;

    // Define Vector Helper functions.
    this.vecSub = (x, y) => [x[0] - y[0], x[1] - y[1]];
    this.vecDot = (x, y) => x[0] * y[0] + x[1] * y[1];
    this.vecSlope = (x, y) => (y[1] - x[1]) / (y[0] - x[0]);
    this.rotatePoint = function (point, angle) {
      return [
        point[0] * Math.cos(angle) - point[1] * Math.sin(angle),
        point[0] * Math.sin(angle) + point[1] * Math.cos(angle)
      ];
    };
    this.ptDist = (x, y) => Math.pow(
      Math.pow(x[0] - y[0], 2) + Math.pow(x[1] - y[1], 2),
      0.5
    );


    var canvasDiv = document.getElementsByClassName("game-container");
    burst = new mojs.Burst({
      left: canvasDiv[0].offsetLeft, top: canvasDiv[0].offsetTop,
      radius: { 0: 100 },
      count: 5,
      children: {
        shape: 'circle',
        radius: 20,
        fill: ['deeppink', 'cyan', 'yellow'],
        strokeWidth: 5,
        duration: 2000
      }
    });

  }

  update() {
    this.velocity[0] += this.acceleration[0];
    this.velocity[1] += this.acceleration[1];
    this.center[0] += this.velocity[0];
    this.center[1] += this.velocity[1];
  }

  updateCircleSize() {
    this.image.height = 100;
    this.image.width = 100;
    this.rockRadius = 35;
    this.rockWidth = 130;

  }
}

class Circle extends GameObject {
  constructor(center, radius, velocity, acceleration, strength, color, image = undefined, textSize = 35) {
    super(OBJECT_CIRCLE, center, velocity, acceleration, color, image);
    this.radius = radius;
    this.strength = strength;
    this.originalStrength = strength;
    this.textSize = textSize;
  }

  isCollideWithObject(object, objectType) {
    if (objectType == OBJECT_RECT) {
      return this.isCollideWithRect(rect);
    } else if (objectType == OBJECT_CIRCLE) {
      return this.isCollideWithCircle(circle);
    } else {
      console.log("Invalid Object Type, Returning False");
      return false;
    }
  }

  isCollideWithRect(rect) {
    // Axis Align the Rectangle.
    let circleCenterAligned = this.vecSub(this.center, rect.center);

    let circleDistanceX = Math.abs(circleCenterAligned[0]);
    let circleDistanceY = Math.abs(circleCenterAligned[1]);

    if (circleDistanceX > (rect.dimensions[0] / 2 + this.radius)) {
      return false;
    }
    if (circleDistanceY > (rect.dimensions[1] / 2 + this.radius)) {
      return false;
    }

    //bottom
    if (circleDistanceX <= rect.dimensions[0] / 2) {
      return true;
    }

    //top
    if (circleDistanceY <= rect.dimensions[1] / 2) {
      return true;
    }

    let cornerDistanceSq = Math.pow(circleDistanceX - rect.dimensions[0] / 2, 2) -
      Math.pow(circleDistanceY - rect.dimensions[1] / 2, 2);
    return (cornerDistanceSq <= Math.pow(this.radius, 2));
  }

  isCollideWithCircle(circle) {
    if (this.ptDist(this.center, circle.center) <= (this.radius + circle.radius)) {
      return true;
    } else {
      return false;
    }
  }

  isOut() {
    if (this.strength <= 0) {
      return true;
    } else {
      return false;
    }
  }

  draw(ctx) {
    ctx.save();
    ctx.beginPath();
    ctx.translate(...this.center);
    ctx.arc(0, 0, this.radius, 0, 2 * Math.PI);
    ctx.fillStyle = this.color;
    ctx.fill();

    if (this.image != undefined) {
      ctx.drawImage(this.image, -this.radius, -this.radius, 2 * this.radius, 2 * this.radius);
    }

    ctx.restore();
  }

  drawStrength(ctx) {

    let imgName = this.image.src.replace(/^.*[\\\/]/, '');
    let imgGiftName = images.gift.replace(/^.*[\\\/]/, '');
    let imgBoostLifeName = images.boostLife.replace(/^.*[\\\/]/, '');
    let textTop = mobileCheck() ? 13 : 17;
    if (imgName != imgGiftName &&
      imgName != imgBoostLifeName) {
      let font = "bold " + this.textSize + "px Oswald, sans-serif";
      ctx.save();
      ctx.fillStyle = "white";
      ctx.font = font;
      ctx.lineWidth = 3;
      ctx.textAlign = "center";
      ctx.strokeText(this.strength, this.center[0], (this.center[1] + textTop));
      ctx.fillText(this.strength, this.center[0], (this.center[1] + textTop));
      ctx.restore();
    }

  }
}

class RectBullet extends GameObject {
  constructor(center, radius, velocity, acceleration, strength, color, image = undefined) {
    super(OBJECT_CIRCLE, center, velocity, acceleration, color, image);
    this.radius = 15;
    this.strength = strength;
    this.originalStrength = strength;
  }

  isCollideWithObject(object, objectType) {
    if (objectType == OBJECT_RECT) {
      return this.isCollideWithRect(rect);
    } else if (objectType == OBJECT_CIRCLE) {
      return this.isCollideWithCircle(circle);
    } else {
      console.log("Invalid Object Type, Returning False");
      return false;
    }
  }

  isCollideWithRect(rect) {
    // Axis Align the Rectangle.
    let circleCenterAligned = this.vecSub(this.center, rect.center);

    let circleDistanceX = Math.abs(circleCenterAligned[0]);
    let circleDistanceY = Math.abs(circleCenterAligned[1]);

    if (circleDistanceX > (rect.dimensions[0] / 2 + this.radius)) {
      return false;
    }
    if (circleDistanceY > (rect.dimensions[1] / 2 + this.radius)) {
      return false;
    }

    if (circleDistanceX <= rect.dimensions[0] / 2) {
      return true;
    }

    if (circleDistanceY <= rect.dimensions[1] / 2) {
      return true;
    }

    let cornerDistanceSq = Math.pow(circleDistanceX - rect.dimensions[0] / 2, 2) -
      Math.pow(circleDistanceY - rect.dimensions[1] / 2, 2);
    return (cornerDistanceSq <= Math.pow(this.radius, 2));
  }

  isCollideWithCircle(circle) {
    if (this.ptDist(this.center, circle.center) <= (this.radius + circle.radius)) {
      return true;
    } else {
      return false;
    }
  }

  isOut() {
    if (this.strength <= 0) {
      return true;
    } else {
      return false;
    }
  }

  draw(ctx) {
    ctx.save();
    ctx.beginPath();
    ctx.translate(...this.center);

    // ctx.arc(0, 0, this.radius, 0, 2 * Math.PI);
    ctx.fillStyle = this.color;
    ctx.fill();

    if (this.image != undefined) {
      if (mobileCheck()) {
        ctx.drawImage(this.image, -this.radius, -this.radius, 10, 20);
      } else {
        ctx.drawImage(this.image, -this.radius, -this.radius, 15, 30);
      }

    }

    ctx.restore();
  }
}

class Rect extends GameObject {
  constructor(center, dimensions, color, image = undefined) {
    super(OBJECT_RECT, center, [0, 0], [0, 0], color, image);
    this.dimensions = dimensions;
  }
  returnDrawingCoordinates() {
    return [- Math.round(this.dimensions[0] / 2),
    - Math.round(this.dimensions[1] / 2),
    this.dimensions[0],
    this.dimensions[1]];
  }
  draw(ctx) {
    ctx.save();
    ctx.beginPath();
    ctx.translate(...this.center);
    ctx.fillStyle = this.color;

    if (this.image != undefined) {
      ctx.drawImage(
        this.image,
        - Math.round(this.dimensions[0] / 2),
        - Math.round(this.dimensions[1] / 2),
        ...this.dimensions
      );

      ctx.fillStyle = "transparent";
    }

    ctx.fillRect(...this.returnDrawingCoordinates());
    ctx.restore();
  }
}

class Game {
  constructor(canvas, images, colors, sounds) {
    let self = this;
    document.addEventListener("keydown", function (e) { self.handleKeyPress(e, self) }, true);
    document.addEventListener("keyup", function (e) { self.stopMotion(e, self) }, true);

    this.canvas = canvas;
    this.ctx = canvas.getContext("2d");
    this.width = this.canvas.width;
    this.height = this.canvas.height;
    this.images = images;
    this.colors = colors;
    this.sounds = sounds;

    gameStarted = false;
    this.isPaused = false;

    this.rocks = [];
    this.bullets = [];
    this.lifeArray = [];
    this.cannon = undefined;
    this.cannonStyle = undefined;
    this.bonusCannon = undefined;
    this.cannonStyleTimer = undefined;

    this.life = 3;
    this.surface = new Rect(
      [Math.round(this.width / 2), Math.round(this.height)],
      [this.width, Math.round(this.height * (1 / 100))],
      this.colors.surface,
      this.images.surface
    );
    this.background = this.images.background;

    this.levelFactor = 0;
    this.levelScore = 80;

    this.minRockStrength = 10;
    let rockSize = Math.round(this.height / 18) > 40 ? 40 : Math.round(this.height / 18);
    this.rockRadius = mobileCheck() ? (this.height / 25) : rockSize;
    this.rockWidth = 150;
    this.rockTextSize = mobileCheck() ? 22 : 35;

    this.maxRockVelocity = [1, 1];
    this.gravity = 0.02;

    this.bulletRadius = 5;
    this.bulletVelocity = [0, -3];
    this.bulletStrength = 1;
    this.bulletAcceleration = [0, 0];

    this.score = 0;
    this.interval = 0;
    this.rockSpawnInterval = 280;
    this.bulletSpawnInterval = 25;
    this.totalLife = this.life;
    gameEndScreen = false;
    // this.createLife();
  }

  showMenu() {
    gameEndScreen = false;
    homepageScreen = true;
    this.ctx.clearRect(0, 0, this.width, this.height);

    if (this.background != undefined) {
      this.ctx.drawImage(this.background, 0, 0, this.width, this.height);
    }

    this.surface.draw(this.ctx);

    if (mobileCheck()) {
      this.ctx.drawImage(this.images.title, (this.width / 2 - 75), 100, 150, 75);

      this.ctx.closePath();
      this.ctx.stroke();
      this.ctx.fill();

      this.ctx.drawImage(this.images.play, (this.width / 2 - 25), 167, 9, 18);
      this.ctx.drawImage(this.images.swipe, (this.width / 2 - 10), 340, 25, 35);
      this.ctx.drawImage(this.images.cannon, (this.width / 2 - 17), this.height - 100, 45, 71);

      this.ctx.fillStyle = "black";
      this.ctx.textAlign = "center";
      this.ctx.font = "bold 30px Oswald, sans-serif";
      this.ctx.fillText("Leader Board", (this.width / 2 + 5), 230);

      this.ctx.fillStyle = "White";
      this.ctx.textAlign = "center";
      this.ctx.font = "bold 26px Oswald, sans-serif";
      this.ctx.fillText("SWIPE TO SHOOT", (this.width / 2 + 5), 325);

    } else {
      this.ctx.drawImage(this.images.title, (this.width / 2 - 150), 100, 309, 150);
    }
    this.ctx.save();
    this.ctx.restore();
  }

  start() {
    gameStarted = true;
    homepageScreen = false;
    gameEndScreen = false;
    this.cannonStyle == undefined;
    this.score = this.totalLife == 0 ? 0 : this.score;
    this.totalLife = this.totalLife == 0 ? this.life : this.totalLife;
    this.interval = 0;
    this.levelFactor = this.totalLife == 0 ? 0 : this.levelFactor;
    this.isPaused = false;
    this.sounds.background.play();

    if (mobileCheck()) {
      this.cannon = new Rect(
        [150, this.canvas.height - 25],
        [35, 50],
        this.colors.cannon,
        this.images.cannon
      );
    } else {
      this.cannon = new Rect(
        [Math.round(this.canvas.height / 2), Math.round(this.canvas.width * (0.96))],
        [45, 71],
        this.colors.cannon,
        this.images.cannon
      );
    }



    this.rocks = [];
    this.bullets = [];
    if (this.totalLife == this.life) {
      this.createLife();
    }
    showGameScreen();
    document.querySelectorAll('.burst-container').forEach(item => {
      item.addEventListener('mousemove', event => {
        this.handleMouseEvent(event, this);
      });
      item.addEventListener('touchstart', this.handleTouchEvent, true);
      item.addEventListener('touchmove', this.handleTouchEvent, true);
      item.addEventListener('touchend', this.handleTouchEvent, true);
      item.addEventListener('touchcancel', this.handleTouchEvent, true);
    })
    // document.addEventListener('mousemove', this.handleMouseEvent);
  }

  handleTouchEvent(e) {
    if (e.touches.length === 0) return;
    e.preventDefault();
    e.stopPropagation();
    var touch = e.touches[0];
    // ship.style.left = (touch.pageX - ship.width / 2) + 'px';
    let gameDiv = document.querySelector(".game-container");
    let leftOffset = gameDiv.offsetLeft;
    if (touch.pageX > leftOffset) {
      // console.log(e.pageX);
      game.cannon.center[0] = touch.pageX - leftOffset;
    }
  }

  handleMouseEvent(e, game) {

    let gameDiv = document.querySelector(".game-container");
    let leftOffset = gameDiv.offsetLeft;
    if (e.pageX > leftOffset) {
      // console.log(e.pageX);
      game.cannon.center[0] = e.pageX - leftOffset;
    }

    // ship.style.left = (e.pageX - ship.width / 2) + 'px';
  }

  createLife() {
    let leftOffset = Math.round(this.canvas.width / 13);
    let leftStart = Math.round(this.canvas.width / 13);
    let remainingLife = 0;
    let topPosition = Math.round(this.canvas.width / 13);
    if (mobileCheck()) {
      leftOffset = 30;
      leftStart = 30;
      topPosition = 50;
    }
    if (window.innerHeight == 568) {
      leftOffset = 50;
      leftStart = 40;
      topPosition = 50;
    }
    for (let i = 1; i <= this.life; i++) {
      let height = Math.round(this.canvas.width / 18);
      let width = Math.round(this.canvas.width / 18);

      if (mobileCheck()) {
        height = 25;
        width = 25;
      }
      if (innerHeight == 568) {
        height = 35;
        width = 35;
      }
      this.lifeArray.push(new Rect(
        [leftStart, topPosition],
        [height, width],
        this.colors.cannon,
        this.images.life
      ));
      leftStart += leftOffset;
      remainingLife++;
    }
  }

  removeLife() {
    this.lifeArray.splice(this.totalLife, 1);

    let leftOffset = Math.round(this.canvas.width / 13);
    let leftStart = Math.round(this.canvas.width / 13);
    let topPosition = Math.round(this.canvas.width / 13);
    let lifeCount = 1;
    if (mobileCheck()) {
      leftOffset = 30;
      leftStart = 30;
      topPosition = 50;
    }
    if (window.innerHeight == 568) {
      leftOffset = 50;
      leftStart = 40;
      topPosition = 50;
    }
    for (let i = 1; i <= this.life; i++) {
      let height = Math.round(this.canvas.width / 18);
      let width = Math.round(this.canvas.width / 18);
      if (mobileCheck()) {
        height = 25;
        width = 25;
      }
      if (innerHeight == 568) {
        height = 35;
        width = 35;
      }
      if (lifeCount > this.totalLife) {
        this.lifeArray.push(new Rect(
          [leftStart, topPosition],
          [height, width],
          this.colors.cannon,
          this.images.emptyLife
        ));
      }
      leftStart += leftOffset;
      lifeCount++;
    }

    if (this.totalLife == 0 || this.totalLife <= 0) {
      this.totalLife = 0;
    }
  }

  addLife() {
    this.lifeArray.splice(this.totalLife + 1, 1);
    let lifeCount = 1;
    let leftOffset = Math.round(this.canvas.width / 13);
    let leftStart = Math.round(this.canvas.width / 13);
    let topPosition = Math.round(this.canvas.width / 13);
    if (mobileCheck()) {
      leftOffset = 30;
      leftStart = 30;
      topPosition = 50;
    }
    if (window.innerHeight == 568) {
      leftOffset = 50;
      leftStart = 40;
      topPosition = 50;
    }

    for (let i = 1; i <= this.life; i++) {
      if (lifeCount == this.totalLife + 1) {

        let height = Math.round(this.canvas.width / 18);
        let width = Math.round(this.canvas.width / 18);
        if (mobileCheck()) {
          height = 25;
          width = 25;
        }
        if (innerHeight == 568) {
          height = 35;
          width = 35;
        }
        this.lifeArray.push(new Rect(
          [leftStart, topPosition],
          [height, width],
          this.colors.cannon,
          this.images.life
        ));
      }
      leftStart += leftOffset;
      lifeCount++;
    }

    if (this.totalLife < this.life) {
      this.totalLife++;
    }

    if (this.totalLife == 0 || this.totalLife <= 0) {
      this.totalLife = 0;
    }
  }

  checkCollisions() {
    // Update Bullets
    for (let i = 0; i < this.bullets.length; i++) {
      this.bullets[i].update();
      if (this.bullets[i].center[1] < 0 || this.bullets[i].isOut()) {
        this.bullets.splice(i, 1);
      }
    }

    // Check Collisions
    for (let i = 0; i < this.rocks.length; i++) {
      this.rocks[i].update();

      // Rock and bullets
      for (let b = 0; b < this.bullets.length; b++) {
        if (this.rocks[i].isCollideWithCircle(this.bullets[b])) {
          this.rocks[i].strength -= this.bullets[b].strength;
          this.rocks[i].radius -= 1;
          this.score += this.bullets[b].strength;
          this.bullets.splice(b, 1);
          this.rocks[i].updateCircleSize();
        }
      }

      let imgBoostLifeName = images.boostLife.replace(/^.*[\\\/]/, '');
      let imgGiftLifeName = images.gift.replace(/^.*[\\\/]/, '');
      let rockImg = this.rocks[i].image.src.replace(/^.*[\\\/]/, '');
      let isCollide = (this.rocks[i].isCollideWithRect(this.surface) && (rockImg == imgBoostLifeName || rockImg == imgGiftLifeName));
      // Check if Rock is Out
      if (this.rocks[i].isOut() || isCollide) {
        let rockPostionX = this.rocks[i].center[0];
        let rockPostionY = this.rocks[i].center[1];

        burst
          .tune({ x: rockPostionX, y: rockPostionY })
          .setSpeed(3)
          .replay();

        if (rockImg == imgBoostLifeName) {
          this.addLife();
        } else if (rockImg == imgGiftLifeName) {
          this.bonusCannon = true;
          this.cannonStyleTimer = true;
        }

        if (
          this.rocks[i].originalStrength >= this.minRockStrength * Math.pow(2, this.levelFactor - 3) &&
          this.rocks[i].originalStrength >= 2 * this.minRockStrength
        ) {

          if (this.score > 100 && Math.random() > 0.2 && !this.cannonStyleTimer) {
            this.rocks.push(
              // center, radius, velocity, acceleration, strength, color, image
              new Circle(
                [
                  this.rocks[i].center[0] - Math.round(this.rocks[i].radius / 1.4),
                  this.rocks[i].center[1]
                ],
                Math.round(this.rocks[i].radius / 1.4),
                [
                  - this.rocks[i].velocity[0],
                  - Math.abs(this.rocks[i].velocity[1])
                ],
                this.rocks[i].acceleration,
                1,
                'transparent',
                // this.rocks[i].image
                this.images.gift,
                this.rockTextSize
              )
            );
          } else if (this.score > 10 && Math.random() < 1 && this.life > this.totalLife) {
            this.rocks.push(
              // center, radius, velocity, acceleration, strength, color, image
              new Circle(
                [
                  this.rocks[i].center[0] - Math.round(this.rocks[i].radius / 1.4),
                  this.rocks[i].center[1]
                ],
                Math.round(this.rocks[i].radius / 1.4),
                [
                  - this.rocks[i].velocity[0],
                  - Math.abs(this.rocks[i].velocity[1])
                ],
                this.rocks[i].acceleration,
                1,
                'transparent',
                // this.rocks[i].image
                this.images.boostLife,
                this.rockTextSize
              )
            );
          } else {
            this.rocks.push(
              // center, radius, velocity, acceleration, strength, color, image
              new Circle(
                [
                  this.rocks[i].center[0] - Math.round(this.rocks[i].radius / 1.4),
                  this.rocks[i].center[1]
                ],
                Math.round(this.rocks[i].radius / 1.4),
                [
                  - this.rocks[i].velocity[0],
                  - Math.abs(this.rocks[i].velocity[1])
                ],
                this.rocks[i].acceleration,
                3,
                'transparent',
                // this.rocks[i].image
                this.images.rock1,
                this.rockTextSize
              ),
              new Circle(
                [
                  this.rocks[i].center[0] + Math.round(this.rocks[i].radius / 1.4),
                  this.rocks[i].center[1]
                ],
                Math.round(this.rocks[i].radius / 1.4),
                [
                  this.rocks[i].velocity[0],
                  - Math.abs(this.rocks[i].velocity[1])
                ],
                this.rocks[i].acceleration,
                3,
                this.rocks[i].color,
                this.images.rock2,
                this.rockTextSize
              ),
            );
          }
        }

        this.rocks.splice(i, 1);
        continue;
      }

      // Rock and Cannon
      if (this.rocks[i].isCollideWithRect(this.cannon)) {
        this.totalLife--;
        this.removeLife();
        this.bonusCannon = false;
        if (this.totalLife == 0) {
          this.stop();
        } else {
          this.start();
        }

        break;
      }

      // Rock and top and bottom surface
      if (
        this.rocks[i].isCollideWithRect(this.surface)
      ) {
        this.rocks[i].center[1] = this.surface.center[1] - this.surface.dimensions[1] / 2 - this.rocks[i].radius;
        this.rocks[i].velocity[1] = -this.rocks[i].velocity[1];
      }
      if (
        this.rocks[i].center[1] - this.rocks[i].radius < 0
      ) {
        this.rocks[i].center[1] = this.rocks[i].radius;
        this.rocks[i].velocity[1] = -this.rocks[i].velocity[1];
      }

      // Rock and sides of canvas
      if (
        this.rocks[i].center[0] - this.rocks[i].radius < 0
      ) {
        this.rocks[i].center[0] = this.rocks[i].radius;
        this.rocks[i].velocity[0] = -this.rocks[i].velocity[0];
      }
      if (
        this.rocks[i].center[0] + this.rocks[i].radius > this.width
      ) {
        this.rocks[i].center[0] = this.width - this.rocks[i].radius;
        this.rocks[i].velocity[0] = -this.rocks[i].velocity[0];
      }
    }

    // Check if cannon collided with sides
    if (this.cannon.center[0] - this.cannon.dimensions[0] / 2 < 0) {
      this.cannon.center[0] = Math.round(this.cannon.dimensions[0] / 2);
    }
    if (this.cannon.center[0] + this.cannon.dimensions[0] / 2 > this.width) {
      this.cannon.center[0] = this.width - Math.round(this.cannon.dimensions[0] / 2);
    }
  }

  createRock() {
    let mathRandom = Math.random();
    if (mathRandom > 0.4 && mathRandom < 0.5) {
      // Left Rock
      this.rocks.push(
        // center, radius, velocity, acceleration, strength, color, image
        new Circle(
          [
            this.rockRadius,
            Math.round(Math.random() * (this.rockWidth / 2 - this.rockRadius)) + this.rockRadius
          ],
          this.rockRadius,
          [
            (0.5 + Math.random() / 2) * this.maxRockVelocity[0],
            (2 * Math.random() - 1) * this.maxRockVelocity[1]
          ],
          [
            0,
            this.gravity
          ],
          this.minRockStrength * Math.pow(2, this.levelFactor),
          this.colors.rock[Math.floor(Math.random() * this.colors.rock.length)],
          this.images.rock,
          this.rockTextSize
        )
      );
    } else if (mathRandom > 0.5 && mathRandom < 0.6) {
      this.rocks.push(
        // center, radius, velocity, acceleration, strength, color, image
        new Circle(
          [
            this.width - this.rockRadius,
            Math.round(Math.random() * (this.width / 2 - this.rockRadius)) + this.rockRadius
          ],
          this.rockRadius,
          [
            - (0.5 + Math.random() / 2) * this.maxRockVelocity[0],
            (2 * Math.random() - 1) * this.maxRockVelocity[1]
          ],
          [
            0,
            this.gravity
          ],
          this.minRockStrength * Math.pow(2, this.levelFactor),
          this.colors.rock[Math.floor(Math.random() * this.colors.rock.length)],
          this.images.rock1,
          this.rockTextSize
        )
      );
    } else if (mathRandom > 0.6 && mathRandom < 0.7) {
      this.rocks.push(
        // center, radius, velocity, acceleration, strength, color, image
        new Circle(
          [
            this.width - this.rockRadius,
            Math.round(Math.random() * (this.width / 2 - this.rockRadius)) + this.rockRadius
          ],
          this.rockRadius,
          [
            - (0.5 + Math.random() / 2) * this.maxRockVelocity[0],
            (2 * Math.random() - 1) * this.maxRockVelocity[1]
          ],
          [
            0,
            this.gravity
          ],
          this.minRockStrength * Math.pow(2, this.levelFactor),
          this.colors.rock[Math.floor(Math.random() * this.colors.rock.length)],
          this.images.rock2,
          this.rockTextSize
        )
      );
    } else {
      // Right Rock
      this.rocks.push(
        // center, radius, velocity, acceleration, strength, color, image
        new Circle(
          [
            this.width - this.rockRadius,
            Math.round(Math.random() * (this.width / 2 - this.rockRadius)) + this.rockRadius
          ],
          this.rockRadius,
          [
            - (0.5 + Math.random() / 2) * this.maxRockVelocity[0],
            (2 * Math.random() - 1) * this.maxRockVelocity[1]
          ],
          [
            0,
            this.gravity
          ],
          this.minRockStrength * Math.pow(2, this.levelFactor),
          this.colors.rock[Math.floor(Math.random() * this.colors.rock.length)],
          this.images.rock,
          this.rockTextSize
        )
      );
    }
  }

  createBullet() {
    this.bullets.push(
      // center, radius, velocity, acceleration, strength, color, image
      new RectBullet(
        [
          this.cannon.center[0],
          this.cannon.center[1] - Math.round(this.cannon.dimensions[1]),
        ],
        this.bulletRadius,
        this.bulletVelocity,
        this.bulletAcceleration,
        this.bulletStrength * Math.pow(2, this.levelFactor),
        this.colors.bullet,
        this.images.bullet
      )
    );
    if (this.cannonStyle == "style2") {
      this.bullets.push(
        // center, radius, velocity, acceleration, strength, color, image
        new RectBullet(
          [
            this.cannon.center[0] + 25,
            this.cannon.center[1] - Math.round(this.cannon.dimensions[1]),
          ],
          this.bulletRadius,
          this.bulletVelocity,
          this.bulletAcceleration,
          this.bulletStrength * Math.pow(2, this.levelFactor),
          this.colors.bullet,
          this.images.bullet
        )
      );
    }

  }

  update() {
    if (gameStarted == false || this.isPaused == true) return;
    this.cannon.update();
    this.checkCollisions();

    this.interval++;
    if (this.interval % this.bulletSpawnInterval == 0) {
      this.createBullet();
    }

    if (this.interval == this.rockSpawnInterval) {
      this.createRock();
      this.interval = 0;
    }

    if (this.score == this.levelScore * Math.pow(2, this.levelFactor)) {
      this.levelFactor++;
    };

    let self = this;
    if (FPS == 0) {
      setZeroTimeout(function () {
        self.update();
      });
    } else {
      setTimeout(function () {
        self.update();
      }, 1000 / FPS);
    }
  }

  display() {
    if (gameStarted == false) return;
    this.ctx.clearRect(0, 0, this.width, this.height);

    if (this.background != undefined) {
      this.ctx.drawImage(this.background, 0, 0, this.width, this.height);
    }

    //center heading
    if (mobileCheck()) {
      this.ctx.drawImage(this.images.title, Math.round((this.width / 2) - 50), this.height * 0.05, 90, 45);
    } else {
      this.ctx.drawImage(this.images.title, Math.round((this.width / 2) - 60), this.height * 0.05, 103, 50);
    }


    //timer
    if (mobileCheck()) {
      this.ctx.drawImage(this.images.timer, Math.round((this.width / 1.49)), this.height * 0.05, 25, 26);
      document.querySelector(".timer").setAttribute("style", "left:" + (Math.round((this.width / 1.49)) + 4) + "px;top:" + (this.height * 0.03 + 9) + "px;");
    } else if (window.innerHeight == 568) {
      this.ctx.drawImage(this.images.timer, Math.round((this.width / 1.55)), this.height * 0.05, 35, 38);
      document.querySelector(".timer").setAttribute("style", "left:" + (Math.round((this.width / 1.55)) + 10) + "px;top:" + (this.height * 0.05 + 17) + "px;");
    } else {
      this.ctx.drawImage(this.images.timer, Math.round((this.width / 1.43)), this.height * 0.05, 45, 53);
      document.querySelector(".timer").setAttribute("style", "left:" + (Math.round((this.width / 1.43)) + 10) + "px;top:" + (this.height * 0.05 + 17) + "px;");
    }


    //pointbg
    if (mobileCheck()) {
      this.ctx.drawImage(this.images.pointBg, Math.round((this.width / 1.5)), this.height * 0.1, 108, 41);
    } else if (window.innerHeight == 568) {
      this.ctx.drawImage(this.images.pointBg, Math.round((this.width / 1.38)), this.height * 0.05, 90, 41);
    } else {
      this.ctx.drawImage(this.images.pointBg, Math.round((this.width / 1.3)), this.height * 0.05, 108, 41);
    }

    this.surface.draw(this.ctx);

    //canon     
    if (this.bonusCannon == true) {
      this.bonusCannon = false;
      this.cannonStyle = "style2";
      if (this.cannonStyle == "style2") {
        let self = this;
        setTimeout(function () {
          self.cannonStyle = undefined;
          self.cannonStyleTimer = false;

          if (mobileCheck()) {
            self.cannon = new Rect(
              [150, self.canvas.height - 25],
              [35, 50],
              self.colors.cannon,
              thself.images.cannon
            );
          } else {
            self.cannon = new Rect(
              [self.cannon.center[0], self.cannon.center[1]],
              [Math.round(300 / 6), Math.round(self.canvas.width / 18)],
              self.colors.cannon,
              self.images.cannon
            );
          }
        }, 10000);
        this.cannonStyle = "style2";

        if (mobileCheck()) {
          this.cannon = new Rect(
            [150, this.canvas.height - 25],
            [35, 50],
            this.colors.cannon,
            this.images.cannon
          );
        } else {
          this.cannon = new Rect(
            [this.cannon.center[0], this.cannon.center[1]],
            [Math.round(300 / 6), Math.round(this.canvas.width / 18)],
            this.colors.cannon,
            this.images.cannon2
          );
        }

      }
    }

    this.cannon.draw(this.ctx);

    for (let lifeObj of this.lifeArray) {
      lifeObj.draw(this.ctx);
    }

    for (let rock of this.rocks) {
      rock.draw(this.ctx);
      rock.drawStrength(this.ctx);
    }

    for (let bullet of this.bullets) {
      bullet.draw(this.ctx);
    }

    this.ctx.save();
    this.ctx.font = "30px Oswald, sans-serif";
    this.ctx.fillStyle = "white";
    this.ctx.textAlign = "center";


    if (mobileCheck()) {
      this.ctx.drawImage(this.images.pointBg, Math.round((this.width / 1.5)), this.height * 0.1, 108, 41);
    } else if (window.innerHeight == 568) {
      this.ctx.drawImage(this.images.pointBg, Math.round((this.width / 1.38)), this.height * 0.05, 90, 41);
    } else {
      this.ctx.drawImage(this.images.pointBg, Math.round((this.width / 1.3)), this.height * 0.05, 108, 41);
    }
    if (mobileCheck()) {
      this.ctx.fillText(this.score, Math.round(this.width / 1.18), this.height * 0.15);
    } else if (window.innerHeight == 568) {
      this.ctx.fillText(this.score, Math.round(this.width / 1.22), this.height * 0.10);
    } else {
      this.ctx.fillText(this.score, Math.round(this.width / 1.18), this.height * 0.09);
    }

    this.ctx.restore();

    let self = this;
    requestAnimationFrame(function () {
      self.display();
    });
  }

  stop() {
    gameStarted = false;
    gameEndScreen = true;
    homepageScreen = false;
    this.cannonStyle == undefined;
    showEndScreen();
    document.getElementById('gameScore').innerHTML = this.score;
    if (this.sounds.explosion != undefined) {
      this.sounds.explosion.play();
    }
    // if (this.score > highScore) {
    //   highScore = this.score;
    //   localStorage.setItem("hs", highScore);
    //   localStorage.setItem("cs", this.score);      
    // }
    totalLife--;
    this.ctx.clearRect(0, 0, this.width, this.height);

    if (this.background != undefined) {
      this.ctx.drawImage(this.background, 0, 0, this.width, this.height);
    }


    this.surface.draw(this.ctx);
    clearInterval(timer);
    document.getElementById('timer').innerHTML = config.settings.timer;
    this.ctx.save();
    this.sounds.background.stop();
  }

  handleKeyPress(e, self) {
    if (gameStarted == true && gameEndScreen == false) {

      if (self.isPaused == false) {
        if (e.keyCode == 37 || e.keyCode == 65) {
          self.cannon.velocity = [-2, 0];
        }
        if (e.keyCode == 39 || e.keyCode == 68) {
          self.cannon.velocity = [2, 0];
        }
        if (e.keyCode == 80) {
          this.isPaused = true;
        }
        if (e.keyCode == 77) {
          this.sounds.background.stop();
        }
      } else {
        if (e.keyCode == 82) {
          self.isPaused = false;
          self.update();
        }
      }
    } else {
      if ((e.keyCode == 39 || e.keyCode == 68) && gameEndScreen == false) {
        self.start();
        self.update();
        self.display();
      }
      // self.start();
      // self.update();
      // self.display();
    }


  }

  stopMotion(e, self) {
    if (gameStarted == true && this.isPaused == false &&
      (e.keyCode == 37 || e.keyCode == 39 || e.keyCode == 65 || e.keyCode == 68)
    ) {
      self.cannon.velocity = [0, 0];
    }
  }
}
function updatetimer(timeSecond) {
  document.getElementById('timer').innerHTML = timeSecond;
  if (timeSecond < 0) {
    clearInterval(timer);
    document.getElementById('timer').innerHTML = 30;
    game.stop();
  }
}
function startGame() {
  let offsetWindowHeight = (window.innerHeight - 72);
  canvas = document.querySelector("#game");
  if (mobileAndTabletCheck()) {
    canvas.setAttribute("height", window.innerHeight);
    canvas.setAttribute("width", window.innerWidth);
    document.querySelector(".game-container").setAttribute("style", "width:" + window.innerWidth + "px;height:" + window.innerHeight + "px;");
  } else {
    canvas.setAttribute("height", offsetWindowHeight);
    canvas.setAttribute("width", offsetWindowHeight);
  }
  loadImages(images, function (imgs) {
    start(imgs);
  });
  hideHomeScreen();
  hideLeaderBoard();
  let start = function (imgs) {
    game = new Game(canvas, imgs, colors, sounds);
    game.start();
    game.update();
    game.display();
  };
  let timeSecond = config.settings.timer;
  timer = setInterval(() => {
    timeSecond--;
    // document.getElementById('gameScore').innerHTML = this.score;
    updatetimer(timeSecond);
  }, 1000)
}